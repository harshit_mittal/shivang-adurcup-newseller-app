package com.shivang.vendor.misc;

/**
 * Created by Sourabh on 24/12/2016.
 */
public class Shape {
    private String overview;
    private String details;

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
