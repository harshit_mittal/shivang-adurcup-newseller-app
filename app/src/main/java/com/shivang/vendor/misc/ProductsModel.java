package com.shivang.vendor.misc;

import java.util.List;

/**
 * Created by Sourabh on 24/12/2016.
 */

public class ProductsModel {
    private List<ProductModel> productModel;

    public List<ProductModel> getProductModel() {
        return productModel;
    }

    public void setProductModel(List<ProductModel> productModel) {
        this.productModel = productModel;
    }
}
