package com.shivang.vendor.misc;

/**
 * Created by Sourabh on 24/12/2016.
 */
public class Size {
    private String unit;
    private String value;
    private String type;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
