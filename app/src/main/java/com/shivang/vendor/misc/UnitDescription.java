package com.shivang.vendor.misc;

import static android.R.attr.value;

/**
 * Created by Sourabh on 24/12/2016.
 */
public class UnitDescription {
    //private Integer value;
    private String unit;
    private String per;

    public Integer getValue() {
        return value;
    }

//    public void setValue(Integer value) {
//        this.value = value;
//    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }
}
