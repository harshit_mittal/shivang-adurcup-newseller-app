package com.shivang.vendor.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shivang.vendor.R;
import com.shivang.vendor.misc.ProductModel;

import java.util.List;
import java.util.Map;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private List<Map> productList;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView product,company,category,identifier1,identifier2;

        MyViewHolder(View view) {
            super(view);
            product = (TextView) view.findViewById(R.id.product_name);
            category = (TextView) view.findViewById(R.id.category);
            company = (TextView) view.findViewById(R.id.company_name);
            identifier1 = (TextView) view.findViewById(R.id.identifier1);
            identifier2 = (TextView) view.findViewById(R.id.identifier2);
        }
    }


    public ProductAdapter(List<Map> productList) {
        this.productList = productList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_product_card_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //TODO set Text to corresponding data coming from Api
        ProductModel product = new ProductModel();
//        product.setVolume((String));
        holder.product.setText((String)productList.get(position).get("productTittle"));
        holder.company.setText(product.getManufacturer());
        holder.category.setText(product.getCategory());

        if(product.getVolume()!=null && product.getVolume().getUnit()!=null && product.getVolume().getValue()!=null)
            holder.identifier1.setText(product.getVolume().getValue() + " " + product.getVolume().getUnit());

        else if(product.getSizeTop()!=null && product.getSizeTop().getType()!=null)
        {
            if(holder.identifier1.getText()==null)
                holder.identifier1.setText(product.getSizeTop().getType() + " " + product.getSizeTop().getValue() + product.getSizeTop().getUnit());
            else
                holder.identifier2.setText(product.getSizeTop().getType() + " " + product.getSizeTop().getValue() + product.getSizeTop().getUnit());
        }


        else if(product.getCompartment() != null)
        {
            if(holder.identifier1.getText()==null)
                holder.identifier1.setText(product.getCompartment());
            else
                holder.identifier2.setText(product.getCompartment());
        }


        else if(product.getGsm()!=null)
        {
            if(holder.identifier1.getText()==null)
                holder.identifier1.setText(product.getGsm());
            else
                holder.identifier2.setText(product.getGsm());
        }

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
