package com.shivang.vendor.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.shivang.vendor.R;
import com.shivang.vendor.activity.AddProductActivity;
import com.shivang.vendor.misc.VolleyController;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class FoodPackagingFragment extends FormFragment {

    private FragmentPagerAdapter adapter;

    public static Fragment newInstance() {
        return new FoodPackagingFragment();
    }

    private VolleyController volleyController;
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        volleyController = VolleyController.getInstance(getContext());
        View rootView = inflater.inflate(R.layout.fragment_services, container, false);

        rootView.findViewById(R.id.add_product).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddProductActivity.class));
            }
        });

        return rootView;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         adapter = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int index) {
                return ActiveFragment.newInstance(index, products);

            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "Active";
                    case 1:
                        return "Out Of Stock";
                    default:
                        return "Inactive";
                }

            }
        };

        pager = (ViewPager) view.findViewById(R.id.pager);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);


        setAllProducts();
    }

    private ViewPager pager;
    private String pDetails;
    private ArrayList products;
    private void setAllProducts()
    {
        StringRequest request = new StringRequest(
                Request.Method.GET, getString(R.string.url_list_current_product),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String stResponse) {
                        dismissProgressDialog();
                        pager.setVisibility(View.VISIBLE);
                        pDetails = stResponse;
                        products = new Gson().fromJson(stResponse, ArrayList.class);
                        pager.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onRequestError(error, null, null);
                    }
                });

        request.setShouldCache(false);
        volleyController.addToRequestQueue(request, TAG);
    }

    @Override
    boolean isErrorActive() {
        return false;
    }
}
