package com.shivang.vendor.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shivang.vendor.R;

/**
 * Created by kshivang on 22/12/16.
 */

public class OrderFragment extends Fragment {

    public static Fragment newInstance() {
        return new OrderFragment();
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_order, container, false);
    }
}
