package com.shivang.vendor.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.shivang.vendor.R;
import com.shivang.vendor.adapter.ProductAdapter;
import com.shivang.vendor.misc.ProductModel;
import com.shivang.vendor.misc.ProductsModel;
import com.shivang.vendor.misc.ValueUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;
import static android.media.CamcorderProfile.get;

public class ActiveFragment extends Fragment {

    private static final String ARG_FRAGMENT_ID = "frag";
    private static final String ARG_DETAILS_ID = "details";

    public void setProducts(List<Map> products) {
        this.products = products;
    }

    private List<Map> products;

    public static ActiveFragment newInstance(int layoutResId, ArrayList products) {
        ActiveFragment sampleSlide = new ActiveFragment();

//        Gson gson = new GsonBuilder()
//                .registerTypeAdapter(Uri.class, new UriSerializer())
//                .create();
        Bundle args = new Bundle();
        args.putInt(ARG_FRAGMENT_ID, layoutResId);
        sampleSlide.setArguments(args);
        sampleSlide.setProducts(products);
        return sampleSlide;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_active_food_packaging, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

//        Gson gson = new GsonBuilder()
//                .registerTypeAdapter(Uri.class, new UriDeserializer())
//                .create();

        if (getArguments() != null && getArguments().getString(ARG_DETAILS_ID) != null) {
            String stResponse = getArguments().getString(ARG_DETAILS_ID);

           // products = new Gson().fromJson(stResponse, ProductsModel.class);


            // ProductsModel productsModel = gson.fromJson(getArguments().getString(ARG_DETAILS_ID), ProductsModel.class);
//            try {
//
//                JSONArray response = new JSONArray(getArguments().getString(ARG_DETAILS_ID));
//                products = new ArrayList<>();
//                for (int i = 0; i < response.length(); i++) {
//                    ProductModel productModel = new ProductModel();
//
//                    JSONObject object = response.optJSONObject(i);
//
//                    productModel.setProductTitle(object.optString("productTitle"));
//                    productModel.setManufacturer(object.optString("manufacturer"));
//                    productModel.setCategory(object.optString("category"));
//                    ValueUnit volume = new ValueUnit();
//                    JSONObject volumeObject = object.optJSONObject("volume");
//
//                    if (volumeObject != null) {
//                        volume.setUnit(volumeObject.optString("unit", "unit"));
//                        volume.setValue(volumeObject.optString("value", "value"));
//                    } else {
//                        volume.setUnit("unit");
//                        volume.setValue("value");
//                    }
//
//                    productModel.setVolume(volume);
//                    products.add(i, productModel);
//                  //  gson.fromJson(getArguments().getString(ARG_DETAILS_ID), ProductsModel.class);
//                }
////                Log.e(TAG, "onCreateView: " + response);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


            ProductAdapter mAdapter = new ProductAdapter(products);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(rootView.getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } else{
            Log.e(TAG, "onCreateView: ");
        }

        return rootView;
    }

}
