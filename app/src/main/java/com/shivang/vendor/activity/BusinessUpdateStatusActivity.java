package com.shivang.vendor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.shivang.vendor.R;
import com.shivang.vendor.fragment.VendorDetailFormFragment;
import com.shivang.vendor.misc.VolleyController;

/**
 * Created by kshivang on 16/12/16.
 *
 */

public class BusinessUpdateStatusActivity extends FormActivity{

    private VolleyController volleyController;
    private final String TAG = "busi";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_appbar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        volleyController = VolleyController.getInstance(this);

        isRegistrationDetailPending();
    }

    @Override
    public void onStop() {
        super.onStop();
        volleyController.cancelPendingRequests(TAG);
    }

    private void isRegistrationDetailPending() {
        showProgressDialog("Loading..");

        StringRequest request = new StringRequest(Request.Method.GET,
                getString(R.string.url_vendor_business),
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dismissProgressDialog();
                if (response.length() > 2) {
                    startActivity(new Intent(BusinessUpdateStatusActivity.this,
                            NavigationActivity.class));
                    finish();
                } else {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.fragment, VendorDetailFormFragment.newInstance());
                    tx.commit();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onRequestError(error, null, null);
            }
        }){
            @Override
            public Priority getPriority() {
                return Priority.HIGH;
            }
        };
        request.setShouldCache(false);
        volleyController.addToRequestQueue(request, TAG);
    }
}
